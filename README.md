# Google Cloud Platform POC

_This repository is messy. Useful for reference but you'll need to adapt a lot of stuff I did_

## Goal

This POC is about firing up a GKE cluster and install basic services (Traefik and Gitlab runners).

By the way, it is not a real POC as it serves a real purpose: putting my [landing page](https://gitlab.com/fhuitelec/old-landing-page) in a GKE cluster powered by Kubernetes. This is completely overkill I know, but hey, that's how you learn things 😉

## References

- [Quickstart](https://cloud.google.com/container-engine/docs/quickstart)

## Initialize tools

### Get gcloud locally

For more information, go [there](https://cloud.google.com/sdk/downloads#versioned).

```bash
curl https://sdk.cloud.google.com | bash
exec -l $SHELL
gcloud init
```

Few details in `gloud init`:
- Regions and zone: **europe-west1-b** in Belgium

## Create cluster

See [here](https://cloud.google.com/compute/docs/machine-types) for machine types and [here](https://cloud.google.com/compute/pricing#predefined_machine_types) for pricing.

```bash
gcloud container clusters create huitelec \
	--machine-type=g1-small
```

## Initialize kubectl

```bash
gcloud container clusters get-credentials huitelec
```

## Access internal tools

- traefik
`kubectl port-forward -n kube-system (kubectl get pods -n kube-system|grep -m1 traefik-ingress|awk '{print $1}') 8080:8080`

- Kubernetes dashboard
`kubectl proxy`

## Gitlab runner

### Install helm

```bash
brew install kubernetes-helm
helm init
helm repo add gitlab https://charts.gitlab.io
```

### Create namespace

```bash
kubectl create namespace ci
```

### Install helm chart

#### Reference
- [Helm chart docs](https://docs.gitlab.com/ce/install/kubernetes/gitlab_runner_chart.html)
- [gitlab-runner chart repo folder](https://gitlab.com/charts/charts.gitlab.io/tree/master/charts/gitlab-runner)

#### Getting started

- Install the chart
```bash
helm install \
	--namespace ci \
	--name gitlab-runner \
	-f values.yaml \
	--set gitlabUrl=https://gitlab.com/ci,runnerRegistrationToken="YOUR-TOKEN" \
	gitlab-runner-0.1.3.tar.gz
```

#### Troubleshooting

##### Don't follow the docs to the letter

Even if `gitlabUrl` & `runnerRegistrationToken` are specified in `values.yaml`, you have to use the `--set` argument within the `helm install` command.

##### Various issues

At this time, there is an [issue](https://gitlab.com/charts/charts.gitlab.io/issues/40) from the last merge so I had to tar the last release.

FYI: I cloned the repo, hard resetted to the last stable commit (HEAD~1) and tar the gitlab-runner folder. You can specify the tar file to use during the installation like this:

```bash
helm install \
	(...) \
	gitlab-runner-0.1.3.tar.gz
```
